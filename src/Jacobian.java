
class Jacobian extends Package {

    private double [][] jacobiandetJ = new double[4][4];
    private double [] detJ = new double[4];
    private double [][] dNdx = new double[4][4];
    private double [][] dNdy = new double[4][4];

    double[][] getdNdy() {
        return this.dNdy;
    }
    double[] getdetJ(){
        return this.detJ;
    }
    double[][] getdNdx() {
        return this.dNdx;
    }

    Jacobian (Element element, double[][]N, double[][]dNdksi, double[][]dNdeta, boolean setVisible) {

        double [][] jacobian = new double[4][4];

        /* konwersja jakobianu 2D */
        for (int a = 0; a < 4; a++) {
            for (int i = 0; i < 4; i++) {
                jacobian[a][0] += dNdksi[a][i] * element.getNode(i).getX();
                jacobian[a][1] += dNdksi[a][i] * element.getNode(i).getY();
                jacobian[a][2] += dNdeta[a][i] * element.getNode(i).getX();
                jacobian[a][3] += dNdeta[a][i] * element.getNode(i).getY();
            }
        } if (setVisible) printMatrix("Jacobian (dxdksi,dydksi,dxdksi,dydksi)",jacobian,4);

        /* wyznacznik jakobianu */
        for (int z = 0; z < 4; z++) detJ[z] = jacobian[z][0] * jacobian[z][3] - jacobian[z][1] * jacobian[z][2];
        if (setVisible) printVector("detJ",detJ);

        /* jakobian przekształcenia / detJ */
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                jacobiandetJ[i][j] = jacobian[i][j] / detJ[i];
            }
        }

        matrixResult(dNdx,0,dNdksi,dNdeta); // dNdx = Jacob / detJ[r][0] * dNdksi[r][c] + Jacob / detJ[r][1] * dNdeta[r][c];
        if (setVisible) printMatrix("dNdx",dNdx,4);

        matrixResult(dNdy,2,dNdksi,dNdeta); // dNdy = Jacob / detJ[r][2] * dNdksi[r][c] + Jacob / detJ[r][3] * dNdeta[r][c];
        if (setVisible) printMatrix("dNdy",dNdy,4);
    }

    private void matrixResult(double[][] result, int help, double[][]dNdksi, double[][]dNdeta) {
        for (int r = 0; r < 4; r++) {
            for (int c = 0; c < 4; c++) {
                result[c][r] = jacobiandetJ[r][help] * dNdksi[r][c] + jacobiandetJ[r][help+1] * dNdeta[r][c];
            }
        }
    }
}