
import java.lang.*;

class P extends Package {

    private double [] globalP;
    double[] getGlobalP() { return this.globalP; }
    private double alfa;
    private double ambientTemperature; // temperatura otoczenia
    private double[][] shapeFunction = new double[2][2];
    private double detJH, detJV;

    P (int nodeNumber, double alfa, double ambientTemperature, double elementLength, double elementHigh) {
        globalP = new double[nodeNumber];
        this.alfa = alfa;
        this.ambientTemperature = ambientTemperature;
        this.detJH = elementLength/2;
        this.detJV = elementHigh/2;
        double[] mp = {-1,1};
        double[] ksi = {-1/Math.sqrt(3), 1/Math.sqrt(3)};
        for (int r = 0; r < ksi.length; r++) {
            for (int c = 0; c < ksi.length; c++) {
                shapeFunction[c][r] = 0.5 * (1 + mp[c]*ksi[r]);
            }
        }
    }

    //************************************************* lokalna

    double[] setLocalP (Element element) {
        // P = { alfa * {N} * tinf }
        double[] localP = new double[4];
        localP = addVectors(calculateSideways(element.getNode(0), element.getNode(1), 0),localP);
        localP = addVectors(calculateSideways(element.getNode(1), element.getNode(2), 1),localP);
        localP = addVectors(calculateSideways(element.getNode(2), element.getNode(3), 2),localP);
        localP = addVectors(calculateSideways(element.getNode(3), element.getNode(0), 3),localP);

        //printVector("Local P", localP);
        return localP;
    }

    private double[] calculateSideways(Node np, Node nk, int side) {
        double[] quarterLocalP = new double[4];
        if (np.getBC() && nk.getBC()) {
            double[] IPLocalP = new double[4];
            //for (int i = 0; i < 4; i++) IPLocalP[i] = 0.0;
            for (int ip = 0; ip < 2; ip++){ // dla 2 pc
                if (0 == side) { //{1, 1, 0, 0};
                     IPLocalP[0] = shapeFunction[0][ip]*detJH*alfa*ambientTemperature;
                     IPLocalP[1] = shapeFunction[1][ip]*detJH*alfa*ambientTemperature;
                }
                if (1 == side){ //{0, 1, 1, 0};
                    IPLocalP[1] = shapeFunction[0][ip]*detJV*alfa*ambientTemperature;
                    IPLocalP[2] = shapeFunction[1][ip]*detJV*alfa*ambientTemperature;
                }
                if (2 == side) { //{0, 0, 1, 1};
                     IPLocalP[2] = shapeFunction[0][ip]*detJH*alfa*ambientTemperature;
                     IPLocalP[3] = shapeFunction[1][ip]*detJH*alfa*ambientTemperature;
                }
                if (3 == side){ //{1, 0, 0, 1};
                     IPLocalP[0] = shapeFunction[0][ip]*detJV*alfa*ambientTemperature;
                     IPLocalP[3] = shapeFunction[1][ip]*detJV*alfa*ambientTemperature;
                }
                quarterLocalP = addVectors(quarterLocalP, IPLocalP);
            }
        } else {
            for (int p = 0; p < 4; p++)
                quarterLocalP[p] = 0.0;
        }
        return quarterLocalP;
    }

    void localPaggregation(Element element, double[] localP) {
        for (int i = 0; i < localP.length; i++)
            globalP[element.getNode(i).getId()] += localP[i];
    }

    void printGlobalP(String text) {
        System.out.println("\nGLOBAL P " + text);
        for (int i = 0; i < globalP.length; i++)
            System.out.print(globalP[i] + " ");
    }
}