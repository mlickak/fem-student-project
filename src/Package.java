
abstract class Package {

    private static final double epsilon = 1e-10;

    static double[] GaussianElimination(double[][] Matrix, double[] Vector){
        int n = Vector.length;
        for (int p = 0; p < n; p++) {
            // find pivot row and swap
            int max = p;
            for (int i = p + 1; i < n; i++) {
                if (Math.abs(Matrix[i][p]) > Math.abs(Matrix[max][p])) {
                    max = i;
                }
            }
            double[] temp = Matrix[p]; Matrix[p] = Matrix[max]; Matrix[max] = temp;
            double   t    = Vector[p]; Vector[p] = Vector[max]; Vector[max] = t;

            // singular or nearly singular
            if (Math.abs(Matrix[p][p]) <= epsilon) {
                throw new ArithmeticException("Matrix is singular or nearly singular");
            }

            // pivot within Matrix and Vector
            for (int i = p + 1; i < n; i++) {
                double alpha = Matrix[i][p] / Matrix[p][p];
                Vector[i] -= alpha * Vector[p];
                for (int j = p; j < n; j++) {
                    Matrix[i][j] -= alpha * Matrix[p][j];
                }
            }
        }
        // back substitution
        double[] x = new double[n];
        for (int i = n - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < n; j++) {
                sum += Matrix[i][j] * x[j];
            }
            x[i] = (Vector[i] - sum) / Matrix[i][i];
        }
        return x;
    }

    static void printMatrix(String output, double [][]matrix, int rows) {
        System.out.println("\n" + output);
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < matrix.length; c++) {
                System.out.print(matrix[c][r] + "    ");
            }
            System.out.println();
        }
    }

    static void printVector(String output, double []vector) {
        System.out.println("\n" + output);
        for (int i = 0; i < vector.length; i++)
            System.out.print(vector[i] + "    ");
    }

    static double[] unitVectorMultiplication(double [] vector, double ratio) {
        for (int c = 0; c < vector.length; c++)
            vector[c] = vector[c] * ratio;
        return vector;
    }

    static double[] addVectors(double[]v1, double[]v2) {
        for (int i=0; i<v1.length; i++)
            v1[i] = v1[i]+v2[i];
        return v1;
    }

    static double[][] add2MatrixOfSameDimension(double[][]m1, double[][]m2, int rows) {
        double [][] result = new double[m1.length][rows];
        for (int c = 0; c < m1.length; c++)
            for (int r = 0; r < rows; r++) {
                result[c][r] = m1[c][r] + m2[c][r];
            }
        return result;
    }

    static double[][] unitMatrixMultiplication(double [][] matrix, double ratio) {
        double[][] result = matrix;
        for (int r = 0; r < matrix.length; r++)
            for (int c = 0; c < matrix.length; c++) {
                result[c][r] = matrix[c][r] * ratio;
            }
        return result;
    }

    static double[][] matrixTransposition(double [][] matrix, int rows) {

        double[][] transposition = new double[rows][matrix.length];

        for (int row = 0; row < rows; row++)
            for (int column = 0; column < matrix.length; column++)
                transposition[row][column] = matrix[column][row];

        return transposition;
    }

    static double[][] matrixMultiplication(double[][]m1, int m1columns, int m1rows, double[][]m2, int m2columns, int m2rows) {

        if (m1columns != m2rows) {
            System.out.println("MATRIX ERROR IntegrationPoint");
            System.exit(0);
        }
        double[][] multi = new double[m1rows][m2columns];
        for (int i = 0; i < m1rows; i++)
            for (int j = 0; j < m2columns; j++) {
                for (int e = 0; e < m1columns; e++) {
                    multi[i][j] += m1[e][j] * m2[i][e];
                }
            }
        return multi;
    }
}
