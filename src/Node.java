
class Node {

    private int id;
    private double x;
    private double y;
    private boolean BC;

    Node (int NodeID, double x, double y, boolean boundaryCondition) {
        this.id = NodeID;
        this.x = x;
        this.y = y;
        this.BC = boundaryCondition;
    }

    double getX () {
        return this.x;
    }

    double getY () {
        return this.y;
    }

    boolean getBC () {
        return this.BC;
    }

    int getId(){
        return this.id;
    }
}