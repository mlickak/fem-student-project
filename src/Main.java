
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.lang.*;

public class Main {

    private static int integerVariables = 4;
    private static int doubleVariables = 8;
    private static int[] I = new int[integerVariables]; // nH, nL;
    private static double[] D = new double[doubleVariables]; // H, L, temp, k, c, ro

    public static void main(String[] args) throws FileNotFoundException {

        String path;
        Scanner initial = new Scanner(System.in);
        System.out.println("Copy and paste below \"Data\" path from MES src file " +
                "(with double \"\\\") example -> C:\\\\Users\\\\Kasia\\\\Desktop\\\\java\\\\mes - kopia zapasowa 12\\\\src\\\\data.txt");
        //path = initial.nextLine();
        path = "C:\\Users\\Kasia\\Desktop\\mes FINAL\\src\\data.txt";
        System.out.println("Data path : " + path + "\n");

        File data = new File(path);
        Scanner text = new Scanner(data);

        int setLineNumber = 0;
        int Integer = 0;
        String description = "empty";

        while (text.hasNextLine()) {
            setLineNumber++;
            if (setLineNumber % 2 != 0) {
                description = text.nextLine();
            }
            else {
                if (setLineNumber <= integerVariables * 2) {
                    I[Integer] = java.lang.Integer.parseInt(text.nextLine());
                    System.out.println("\"" + description + "\" = " + I[Integer] +" loaded as Integer ");
                    Integer++;
                    if (Integer == integerVariables) Integer = 0;
                }
                else { // (setLineNumber <= doubleVariables * 2)
                    D[Integer] = Double.parseDouble(text.nextLine());
                    System.out.println("\"" + description + "\" = " + D[Integer] + " loaded as Double ");
                    Integer++;
                    if (Integer == doubleVariables) Integer = 0;
                }
            }
        }
        Grid grid = new Grid(I[0],I[1],I[2],I[2],D[0],D[1],D[2],D[3],D[4],D[5],D[6],D[7]);

        //Grid test1 = new Grid(500,50,3,3,0.1,0.1,100,25,700,7800,1200,300);
        //Grid test2 = new Grid(20,1,30,30,0.1,0.1,100,25,700,7800,1200,300);
    }
}