
class Element {

    private Node[] nodes = new Node[4];
    private double[][] localH;
    private double[][] localHBC;
    private double[] localP;
    private double[][] localC;

        /* nodes :
     * left down    = 0
     * right down   = 1
     * right top    = 2
     * left top     = 3 */

    Element (Node left_down, Node left_top, Node right_top, Node right_down) {
        nodes[0] = left_down;
        nodes[1] = right_down;
        nodes[2] = right_top;
        nodes[3] = left_top;
    }

    Node getNode(int NodeID) { return this.nodes[NodeID];}

    void setLocalC(double [][] C) { this.localC = C; }

    void setLocalH(double[][]H){ this.localH = H; }

    void setLocalHBC(double[][] localHBC) { this.localHBC = localHBC; }

    void setLocalP(double[]P) { this.localP = P; }

    double[][] getLocalH() { return this.localH; }

    double[] getLocalP() { return this.localP; }

    double[][] getLocalC() { return this.localC; }

    double[][] getLocalHBC() { return this.localHBC; }
}