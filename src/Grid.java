
class Grid extends Package {

    private int NH;
    private int NB;
    private int NOE; // number Of Elements;
    private int NON; // number Of Nodes;
    private Element[] element;
    private Node [] node;

    Grid(int simulationTime, int dTau, int NH, int NB,
         double H, double L, double initialTemperature, double conductivity, double c, double ro,
         double ambientTemperature, double alfa) {

        this.NH = NH;
        this.NB = NB;
        NOE = this.NB * this.NH;
        NON = (this.NB + 1) * (this.NH + 1);
        node = new Node[NON];
        element = new Element[NOE];
        double elementHigh = H / this.NH;
        double elementLength = L / this.NB;

        enterNodes(elementHigh, elementLength);
        enterElementsNodes();

        Universal universalElement = new Universal(false);
        H elementH = new H(conductivity, alfa, elementLength, elementHigh, NON);
        C elementC = new C(c, ro, NON);
        P elementP = new P(NON, alfa, ambientTemperature, elementLength, elementHigh);

        for (int e = 0; e < NOE; e++) {
            Jacobian jacobian2D = new Jacobian(
                    element[e],
                    universalElement.getN(),
                    universalElement.getdNdksi(),
                    universalElement.getdNdeta(),
                    false);
            element[e].setLocalH(elementH.setLocalH(
                    jacobian2D.getdNdx(),
                    jacobian2D.getdNdy(),
                    jacobian2D.getdetJ(),
                    universalElement.getN()
            ));
            element[e].setLocalHBC(elementH.setLocalHBC(
                    element[e]));
            element[e].setLocalP(elementP.setLocalP(element[e]));
            element[e].setLocalC(elementC.setLocalC(
                    universalElement.getN(),
                    jacobian2D.getdetJ()
            ));
            elementH.localHAggregation(element[e],element[e].getLocalH());
            elementC.localCAggregation(element[e],element[e].getLocalC());
            elementP.localPaggregation(element[e],element[e].getLocalP());
        }
        //elementH.printGlobalH("BEZ WARUNKÓW BRZEGOWYCH");
        for (int e = 0; e < NOE; e++)
            elementH.localHBCAggregation(element[e],element[e].getLocalHBC());
        //elementH.print("Z WARUNKAMI BRZEGOWYMI");
        //elementC.print();

        double[] pastTemperature = new double[NON];
        for (int i = 0; i < NON; i++)
            pastTemperature[i] = initialTemperature;
        double[] soughtTemperature;
        int iteration = 0;

        String[] summary = new String[simulationTime/dTau+1];

        /* [C] / dTau */
        double[][] globalCdTau = elementC.getGlobalC();
        globalCdTau = unitMatrixMultiplication(globalCdTau, 1.0 / dTau); // dTau


        for (int timePast = dTau; timePast <= simulationTime; timePast += dTau) {
            System.out.println("\n\nITERATION " + iteration);

            /* -([C] / dTau)*(t0) */
            double[] resultVector = GaussEquation(globalCdTau, pastTemperature);
            resultVector = addVectors(resultVector, elementP.getGlobalP());
            //printVector("\n([C] / dTau)*(t0) + P", resultVector);

            /* [H] + [C] / dTau */
            double[][] newGlobalH = add2MatrixOfSameDimension(elementH.getGlobalH(), globalCdTau, NON);
            //printMatrix("\n[H] + [C] / dTau", newGlobalH, newGlobalH.length);

            // szukana temperatura
            soughtTemperature = GaussianElimination(newGlobalH, resultVector);

            // podsumowanie iteracji
            summary[iteration] = timePast + "       " + getMinTemp(soughtTemperature) + "       " + getMaxTemp(soughtTemperature);
            for (int i = 0; i < soughtTemperature.length; i++) {
                pastTemperature[i] = soughtTemperature[i];
                soughtTemperature[i] = 0.0;
            }
            iteration++;
        }

        System.out.println("\n\nTime[s]    MinTemp[C]    MaxTemp[C]");
        for (int s = 0; s < summary.length; s++) {
            if (null == summary[s]) break;
            System.out.println(summary[s]);
        }

        // end
    }

    private double[] GaussEquation (double[][]matrix, double[] vector){
        double[] result = new double[matrix.length];
        if (matrix.length == vector.length) {
            for (int w = 0; w < matrix.length; w++){
                for (int a = 0; a < matrix.length; a++) {
                    result[w] += matrix[a][w] * vector[a];
                }
            }
        } else {
            System.out.println("ERROR GAUSS");
            System.exit(0);
        }
        return result;
    }

    private void enterNodes (double eh, double el) {
        int setterNodeNumber = 0;
        double setterNodeLength = 0.0, setterNodeHigh;
        boolean BC;

        for (int row = 0; row < NB +1; row++) {
            setterNodeHigh = 0.0;
            for (int column = 0; column < NH +1; column++) {
                if (row == 0 || row == NB || column == NH || column == 0) BC = true;
                else BC = false;

                Node temporaryNode = new Node(setterNodeNumber,setterNodeLength,setterNodeHigh,BC);
                node[setterNodeNumber] = temporaryNode;

                /*nastepny numer węzła*/
                setterNodeNumber++;
                /*y nastepnego*/
                setterNodeHigh = setterNodeHigh + eh;
            }
            /*x nastepnego*/
            setterNodeLength = setterNodeLength + el;
        }
    }

    private void enterElementsNodes() {
        int columnEnd = 0, nodeId = 0;
        for (int elementSetter = 0; elementSetter < NOE; elementSetter++) {
            Element temp_element = new Element(node[nodeId],node[nodeId+1],node[nodeId+ NH +2],node[nodeId+ NH +1]);
            element[elementSetter] = temp_element;
            nodeId++;
            columnEnd++;
            if (columnEnd == NH) {
                columnEnd = 0;
                nodeId++;
            }
        }
    }

    void printElementsNodes() {
        for (int i = 0; i < NOE; i++) {
            System.out.println("\nElement " + i + " : ");
            for (int t = 0; t < 4; t++) {
                System.out.print(element[i].getNode(t) + " ");
            }
        }
    }

    void printElementsPosition() {
        for (int i = 0; i < NOE; i++) {
            System.out.println("\nElement " + i + " : ");
            for (int j = 0; j < 4; j++) System.out.println("\nx:  " + element[i].getNode(j).getX() +
                    ", y: " + element[i].getNode(j).getY());
        }
    }

    void printNodes() {
        int temp = 0;
        do {
            System.out.println("Node " + (temp) + " : x = " +
                    node[temp].getX() + ", y = " + node[temp].getY() + ", BC = " + node[temp].getBC());
            temp++;
        } while (temp < NOE);
    }

    private double getMinTemp (double[] array) {
        double min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) min = array[i];
        }
        return min;
    }

    private double getMaxTemp(double[] array) {
        double max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max)
                max = array[i];
        }
        return max;
    }
}