
import java.lang.*;

public class H extends Package {

    private double [][] globalH;
    double[][] getGlobalH() { return this.globalH; }
    private double conductivity;
    private double alfa;
    private double[][] shapeFunction = new double[2][2];
    private double detJH, detJV;

    H (double conductivity, double alfa, double elementLength, double elementHigh, int size) {
        this.conductivity = conductivity;
        this.alfa = alfa;
        double[] mp = {-1,1};
        double[] ksi = {-1/Math.sqrt(3), 1/Math.sqrt(3)};
        for (int r = 0; r < ksi.length; r++) {
            for (int c = 0; c < ksi.length; c++) {
                shapeFunction[c][r] = 0.5 * (1 + mp[c]*ksi[r]);
            }
        }
        this.detJH = elementLength/2;
        this.detJV = elementHigh/2;
        globalH = new double[size][size];
    }

    //************************************************* local H

    double[][] setLocalH (double [][] dNdx, double [][] dNdy, double[] detJ, double[][] N) {
        // H = k*(  {dNpc/dx}{dNpc/dx}T   +   {dNpc/dy}{dNpc/dy}T  )*DetJ
        double[][] localH = new double[4][4];
        double[][] dNdxIP = new double[1][4];
        double[][] dNdyIP = new double[1][4];
        for (int IP = 0; IP < 4; IP++) {
            for (int i = 0; i < 4; i++) {
                dNdxIP[0][i] = dNdx[i][IP];
                dNdyIP[0][i] = dNdy[i][IP];
            }
            localH = add2MatrixOfSameDimension(localH, integrationPointH(dNdxIP,detJ[IP]),4);
            localH = add2MatrixOfSameDimension(localH, integrationPointH(dNdyIP,detJ[IP]),4);
        }

        //printMatrix("Local H",localH,4);
        return localH;
    }

    private double[][] integrationPointH(double[][] tab, double detJ) {
        double[][] partial = tab;
        partial = matrixMultiplication(partial,1,4, matrixTransposition(partial,4),4,1);
        unitMatrixMultiplication(partial, detJ);
        unitMatrixMultiplication(partial, conductivity);
        return partial;
    }

    //************************************************* local HBC

    double[][] setLocalHBC (Element element) {
        // { alfa*{N}*{N}T dS } po S
        double[][] localHBC = new double[4][4];
        localHBC = add2MatrixOfSameDimension(localHBC, horizontalHBC(element.getNode(0),element.getNode(1),1),4);
        localHBC = add2MatrixOfSameDimension(localHBC, verticalHBC(element.getNode(1),element.getNode(2),2),4);
        localHBC = add2MatrixOfSameDimension(localHBC, horizontalHBC(element.getNode(2),element.getNode(3),3),4);
        localHBC = add2MatrixOfSameDimension(localHBC, verticalHBC(element.getNode(3),element.getNode(0),4),4);

        //printMatrix("Local HBC",localHBC,4);
        return localHBC;
    }

    private double[][] horizontalHBC (Node np, Node nk, int SIDE){
        double[][] tmpLocalHBC = new double[4][4];
        if (np.getBC() && nk.getBC()) { // jeśli bok leży na brzegu siatki
            double[][] part = new double[4][4]; // macierz pomocnicza
            for (int i = 0; i < 2; i++) { // dla dwóch punktów całkowania
                double[][] tmp = integrationPointHBC(i, detJH); // odbieramy wynik punktu całkowania
                for (int k = 0; k < 2; k++) { // rozwiązanie układu 2x2
                    for (int t = 0; t < 2; t++) {
                        if (1 == SIDE) //{{1, 1, 0, 0}, {1, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
                            part[t][k] = tmp[t][k];
                        if (3 == SIDE) //{{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 1, 1}, {0, 0, 1, 1}};
                            part[t+2][k+2] = tmp[t][k];
                    }
                }
                tmpLocalHBC = add2MatrixOfSameDimension(tmpLocalHBC, part, 4);
            }
        } else {
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++ )
                    tmpLocalHBC[i][j] = 0.0;
        }
        return tmpLocalHBC;
    }

    private double[][] verticalHBC (Node np, Node nk, int SIDE){
        double[][] tmpLocalHBC = new double[4][4];
        if (np.getBC() && nk.getBC()) {// jeśli bok leży na brzegu siatki
            double[][] quarter = new double[4][4]; // macierz pomocnicza
            for (int i = 0; i < 2; i++) { // dla dwóch punktów całkowania
                double[][] tmp = integrationPointHBC(i, detJV); // odbieramy wynik punktu całkowania
                for (int k = 0; k < 2; k++) { // rozwiązanie układu 2x2
                    for (int t = 0; t < 2; t++) {
                        if (2 == SIDE) //{{0, 0, 0, 0}, {0, 1, 1, 0}, {0, 1, 1, 0}, {0, 0, 0, 0}};
                            quarter[t + 1][k + 1] = tmp[t][k];
                        if (4 == SIDE) //{{1, 0, 0, 1}, {0, 0, 0, 0}, {0, 0, 0, 0}, {1, 0, 0, 1}};
                            quarter[t * 3][k * 3] = tmp[t][k];
                    }
                }
                tmpLocalHBC = add2MatrixOfSameDimension(tmpLocalHBC, quarter, tmpLocalHBC.length);
            }
        } else {
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++ )
                    tmpLocalHBC[i][j] = 0.0;
        }
        return tmpLocalHBC;
    }

    private double[][] integrationPointHBC (int IP, double detJ) {
        double[][] HBCinPoint;
        double [][] tmpN = new double[1][2];
        tmpN[0][0] = shapeFunction[IP][0];
        tmpN[0][1] = shapeFunction[IP][1];
        HBCinPoint = matrixMultiplication(tmpN,1,2,matrixTransposition(tmpN,2),2,1);
        unitMatrixMultiplication(HBCinPoint,detJ);
        unitMatrixMultiplication(HBCinPoint,alfa);
        return HBCinPoint;
    }

    //************************************************* global H

    void localHAggregation(Element element, double[][]localH) {
        for (int i = 0; i<4; i++) {
            for (int j = 0; j < 4; j++) {
                globalH[element.getNode(i).getId()][element.getNode(j).getId()] += localH[i][j];
            }
        }
    }

    void localHBCAggregation(Element element, double[][]BC) {
        for (int i = 0; i<4; i++) {
            for (int j = 0; j < 4; j++) {
                globalH[element.getNode(i).getId()][element.getNode(j).getId()] += BC[i][j];
            }
        }
    }

    void printGlobalH(String text) {
        System.out.println("\nGLOBAL H " + text);
        for (int r = 0; r < globalH.length; r++) {
            for (int c = 0; c < globalH.length; c++) {
                System.out.print(globalH[c][r] + " ");
            }
            System.out.println();
        }
    }
}