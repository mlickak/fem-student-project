
import java.lang.*;

public class C extends Package {

    private double[][] globalC;
    private double c;
    private double ro;

    double[][] getGlobalC() {
        return this.globalC;
    }

    C (double c, double ro, int nodeNumber) {
        this.c = c;
        this.ro = ro;
        globalC = new double[nodeNumber][nodeNumber];
        for (int i = 0; i < globalC.length; i++) {
            for (int j = 0; j < globalC.length; j++)
                globalC[i][j] = 0.0;
        }
    }

    double[][] setLocalC(double[][]N, double[]detJ) {
        // C = { c * ro * {N} * {N}T } po V
        double[][] localC = new double[4][4];
        double[][] tmpN = new double[1][4];
        for (int IP = 0; IP < 4; IP++) {
            for (int row = 0; row < 4; row++) tmpN[0][row] = N[row][IP];
            localC = add2MatrixOfSameDimension(localC,integrationPoint(detJ[IP],tmpN),4);
        }
        unitMatrixMultiplication(localC,c);
        unitMatrixMultiplication(localC,ro);

        //printMatrix("localC",localC,4);
        return localC;
    }

    private double[][] integrationPoint(double detJ, double[][]N) {
        double[][] partial = N;
        partial = matrixMultiplication(partial,1,4, matrixTransposition(N,4),4,1);
        unitMatrixMultiplication(partial,detJ);
        return partial;
    }

    void localCAggregation(Element element,double[][]localC) {
        for (int i = 0; i<4; i++) {
            for (int j = 0; j < 4; j++) {
                globalC[element.getNode(i).getId()][element.getNode(j).getId()] += localC[i][j];
            }
        }
    }

    public void printGlobalC () {
        System.out.println("\nGLOBAL C");
        for (int r = 0; r < globalC.length; r++) {
            for (int c = 0; c < globalC.length; c++) {
                System.out.print(globalC[c][r] + " ");
            }
            System.out.println();
        }
    }
}