
import java.lang.*;

class Universal extends Package {

    private double [][] N = new double[4][4];
    private double [][] dNdksi = new double[4][4];
    private double [][] dNdeta = new double[4][4];

    private double [] ksi = {-1/Math.sqrt(3),1/Math.sqrt(3),1/Math.sqrt(3),-1/Math.sqrt(3)};
    private double [] eta = {-1/Math.sqrt(3),-1/Math.sqrt(3),1/Math.sqrt(3),1/Math.sqrt(3)};
    private double [] mppm = {-1.0, 1.0, 1.0, -1.0};
    private double [] mmpp = {-1.0, -1.0, 1.0, 1.0};

    Universal (boolean setVisible) {

        //uniwersalna macierz funkcji ksztaltu elementu czterowezlowego N
        matrixOfThe4NodeElementShapeFunction();
        if (setVisible) printMatrix("N for universal element", N, 4);

        //pochodne funkcji kształtu względem ksi i pochodne funkcji kształtu względem eta
        derivativesOfTheShapeFunction(dNdksi, eta, mppm, mmpp);
        derivativesOfTheShapeFunction(dNdeta, ksi, mmpp, mppm);
        if (setVisible) printMatrix("dN / dksi for universal element", dNdksi,4);
        if (setVisible) printMatrix("dN / deta for universal element", dNdeta, 4);
        if (setVisible) System.out.println("\nUniversal element been calculated");
    }

    private void derivativesOfTheShapeFunction(double[][] derivative, double[] relative, double [] orientation1, double [] orientation2) {
        /*
         * dN/dksi = +-1/4(1+-eta)
         * dN/deta = +-1/4(1+-ksi)
         * */

        for (int row = 0; row < 4; row++)
            for (int column = 0; column < 4; column++)
                derivative[column][row] = orientation1[row] * 0.25 * (1 + orientation2[row] * relative[column]);
    }

    private void matrixOfThe4NodeElementShapeFunction() {
        /*
         * 1/4 * (1+-ksi)(1+-eta) */

        for (int k = 0; k < 4; k++)
            for (int w = 0; w < 4; w++)
                N[w][k] = 0.25 * (1 + mppm[k] * ksi[w]) * (1 + mmpp[k] * eta[w]);

    }

    double[][] getdNdksi(){
        return this.dNdksi;
    }
    double[][] getN(){
        return this.N;
    }
    double[][] getdNdeta(){
        return this.dNdeta;
    }
}